from time import time


class Car():
    nbr_cars = 0
    def __init__(self, length=3, speed=10, save_dist=1):
        self.name = Car.nbr_cars
        Car.nbr_cars += 1
        self.x = 0.0 # front of the car
        self.speed = speed # [m/s]
        self.length = length # [m]
        self.save_dist = save_dist # [m]
        self.spawn_time = time()
        self.kill_time = None
        self.alive = True
        #TODO: add accelaration

    def __repr__(self):
        return f"Car {self.name}, pos: {round(self.x, 1)}, speed: {self.speed}, life_time: {round(self.life_time(), 2)}"

    def stop(self):
        self.speed = 0

    def drive(self, td, prev_car, light):
        drive = False 
        if light.x > self.x and abs(light.x - self.x) < self.save_dist and light.status == 2:
            drive = False
        elif prev_car is None:
            drive = True
        elif abs(prev_car.x - prev_car.length - self.x) > self.save_dist:
            drive = True 
        else:
            drive = False 

        if drive:
            self.speed = 10
            self.x += self.speed * td
        else:
            self.stop()

    def life_time(self):
        return time() - self.spawn_time if self.alive else self.kill_time - self.spawn_time

    def kill(self):
        self.alive = False
        self.kill_time = time()


class Pedestrian():
    nbr_ped = 0 
    def __init__(self):
        self.name = Pedestrian.nbr_ped
        Pedestrian.nbr_ped += 1
        self.spawn_time = time()

    def __repr__(self):
        return f"Ped {self.name}, life_time: {round(self.wait_time(), 2)}"

    def wait_time(self):
        return time() - self.spawn_time 


class TrafficLight():
    def __init__(self, x=100):
        self.x = x
        self.status = 0
        self.request = False
        self.time_1 = 3
        self.time_2 = 10 
        self.time_3 = 10
        self.time_in_status = 0

    def __repr__(self):
       return f"status: {self.status} ({round(self.time_in_status)} seconds)"

    def next(self):
        """
        0: green
        1: green, with request
        2: red
        3: green, red-cooldown
        Note: Red means red for cars and green for pedestrians
        """
        self.status = (self.status + 1) % 4
        self.time_in_status = 0

    def push_button(self):
        self.request = True

    def update(self, td):
        self.time_in_status += td
        if self.status == 0 and self.request:
            self.next()
        if self.status == 1 and self.time_in_status > self.time_1:
            self.next()
        if self.status == 2 and self.time_in_status > self.time_2:
            self.next()
            self.request = False
        if self.status == 3 and self.time_in_status > self.time_3:
            self.next()


