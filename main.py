from time import time
import random
import math
from math import e
from collections import deque

from objects import Car, Pedestrian, TrafficLight


DEBUG = True
n_cars = 0.18
n_peds = 0.18
street_length = 200
print_every = 2


def step(curr_time, td, start, cars, peds, light, life_times_cars, life_times_peds):

    if spawn(n_cars, td):
        add_car(cars)
    if spawn(n_peds, td):
        add_ped(peds, light)

    light.update(td)
    update_cars(cars, td, light)
    update_peds(peds, td, light, life_times_peds)
    remove_cars(cars, life_times_cars)


def add_car(cars):
    if len(cars):
        if cars[-1].x > cars[-1].length:
            cars.append(Car())
            return 1
        else:
            return -1
    else:
        cars.append(Car())


def add_ped(peds, light):
    peds.append(Pedestrian())
    light.push_button()


def update_peds(peds, td, light, life_times_peds):
    if len(peds):
        if light.status == 2:
            for ped in peds:
                life_times_peds.append(ped.wait_time()) 
            peds.clear()


def update_cars(cars, td, light):
    if len(cars):
        prev_car = None
        for car in cars:
            car.drive(td, prev_car, light)
            prev_car = car


def remove_cars(cars, life_times_cars):
    if cars and cars[0].x > street_length:
        removed_car = cars.popleft()
        removed_car.kill()
        life_times_cars.append(removed_car.life_time())

def spawn(n, td):
    p = e**(-n*td) * n*td # we always assume k = 1 for the poission prop
    test = random.random()
    return p > test


def init_sim():
    start = time()
    cars = deque()
    peds = deque()
    light = TrafficLight()
    return start, cars, peds, light


def print_sim(start, curr_time, last_print, cars, peds, light):
    if curr_time - last_print > print_every:
        print(f"\nSimulation status ({round(time() - start)} seconds in):")
        print("Light:", light)
        print("Cars:")
        for car in cars:
            print(car)
        print("Pedestrians:")
        for ped in peds:
            print(ped)

        return time()

    return last_print


def mainloop():
    start, cars, peds, light = init_sim()
    last_print = time()
    td = 0
    life_times_cars = list()
    life_times_peds = list()

    made_request = False

    try:
        while True:
            curr_time = time()
            if DEBUG:
                last_print = print_sim(start, curr_time, last_print, cars, peds, light)
            step(curr_time, td, start, cars, peds, light, life_times_cars, life_times_peds)
            td = time() - curr_time

    except KeyboardInterrupt:
        print(f"Simulation ended: Runtime {round(time() - start, 2)}")
        print("Avg Life time  for Cars:")
        print(sum(life_times_cars)/len(life_times_cars))
        print("\n")
        print("Avg. wait time for Pedestrians:")
        print(sum(life_times_peds)/len(life_times_peds))


if __name__ == '__main__':
   mainloop()
